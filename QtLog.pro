#-------------------------------------------------
#
# Project created by QtCreator 2017-11-21T22:05:56
#
#-------------------------------------------------

QT       += core gui

DEFINES += QT_MESSAGELOGCONTEXT

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtLog
TEMPLATE = app


SOURCES += \
    loghandler.cpp \
    main.cpp

HEADERS  += \
    singleton.h \
    loghandler.h

FORMS    +=
